#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

. splash.sh
function non-roots()
{
    if [ "$USER" = root ]; then
        echo "This script shouldn't be run as root. Aborting."
        echo "Run script like this:-   ./theme_starter.sh"
        exit 1
    fi
}
function give_permisssions()
{
    chmod +x theme.sh
    chmod +x splash.sh 
}
function remove_Permission()
{
    chmod -x splash.sh 
    chmod -x theme.sh
}
function theme_Start()
{   
    non-roots 
    give_permisssions
    
    echo " "
    splash 'Applying Themes'
    . theme.sh
    apply_themes
    echo " "

    #removing Permissioons 
    remove_Permission

}
theme_Start