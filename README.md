# Gnome Setup:

## Introduction:

It's done the perfect way to install applications and set your gnome desktop. The script is divied into two main parts the application installer and the theme switcher.

The script will detect your your Operating system, so if the system is arch or the system is Debain, the application will run and work like a champ. 

**Note: For this Update the Application will run only for Debian System only. The part for Arch System will be coming soon.**

## Parts of Script:

The script is divided into two main parts and one part is run as the sudo and one part is run as a user.

1. Application Installer
2. Theme Apply

Each section is run automatically and once run the done.

## 1 Installing Applications:

The following application will be installed by running the script:

1. Firefox
2. Libre-Office
3. python3
4. htop
5. neofetch
6. wine64
7. timeshift
8. gnome-shell-extensions
9. gnome-tweaks
10. cargo
11. cmake
12. figlet
13. fish
14. kitty
15. pkg-config
16. libfreetype6-dev
17. libfontconfig1-dev
18. libxcb-xfixes0-dev
19. libxkbcommon-dev
20. curl
21. chrome-gnome-shell
22. spotify
23. Android Studio
24. WPS Office
25. GitHub Desktop
26. Only Office
27. Steam
28. Bitwarden
29. Netbeans
30. VS Code
31. MS-Teams
32. Notion
33. WhatsApp-for-linux
34. auto-cpu-freq
35. Google Chrome
36. Alacritty

## Setting Themes:

Now the application are installed the theme will be set now all of the files will be copied from the repo and once downloaded the it will be placed to the write place.

once all of the config files are placed then with different command the themes and wallpaper can be set.

### Themes:

The theme which is activated and the name of the theme is called "Dark-Blue-Sky" 

### Icon Pack:

The icon pack which is used is "Candy Icon Pack".

### Gnome Extensions:

In this Application we are installing and enabling 4 extensions:

1. User shell Theme
2. Virtual Desktop Display Extension
3. Floating Dock
4. Drive Remover Options Extension

All the above gnome extensions are enabled and installed when the script is run.

# Installment Guide:

To install the Application following the command bellow:

## Dependencies:

There are two main dependencies to install enter the following command in the terminal.

### For Debian System:

1. Git:

```bash
sudo apt-get install git
```

1. CURL

```bash
sudo apt-get install curl
```

### For Arch System:

1. Git:

```bash
sudo pacman -S git
```

1. Curl:

```bash
sudo pacman -S curl
```

## Installment:

### Clone Repo:

1. Clone the Repo using the terminal:

```bash
git clone https://github.com/rafay99-epic/Gnome-setup.git
```

Note: Must Clone the repo in the home directory.

1. From the terminal now enter the folder using the command.

```bash
cd Gnome-setup
```

1. Once in the folder then enter the enter the following command to run the script:

```bash
./install.sh
```

You mus run the script as a user and then the script will run, The script have two parts and one part need the root access so the script will ask for the password.

## The Desktop will look like this:
### The Desktop:
![Screenshot from 2021-11-22 11-20-38](https://user-images.githubusercontent.com/82662797/142810578-dcca9d13-2e9c-4869-afe6-434e46848271.png)


### The Icon Pack:
![Screenshot from 2021-11-22 11-20-49](https://user-images.githubusercontent.com/82662797/142810818-9071ffb1-2d50-4662-b106-dbe8048eda2b.png)
![Screenshot from 2021-11-22 11-20-57](https://user-images.githubusercontent.com/82662797/142810822-3f5719cc-9aa3-49db-814a-fb8083ca1815.png)
![Screenshot from 2021-11-22 11-21-01](https://user-images.githubusercontent.com/82662797/142810835-d9094333-964f-4eb7-a039-fdab3b4b7492.png)


### The Terminal:
![Screenshot from 2021-11-22 11-21-14](https://user-images.githubusercontent.com/82662797/142810498-e0f65853-00ab-4a66-ba1a-88d24b01399c.png)


# Contact Information:

Author: Mohammad Abdul Rafay

Email: 99marafay@gmail.com
