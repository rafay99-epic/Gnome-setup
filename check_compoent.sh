#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */
. check_os.sh

#File location for the snap store
SNAP=/var/lib/snapd/snaps

#the file location for debian.
FLATPAK=/usr/bin/flatpak

#Location for the yay on the arch system
YAY_LOCATION=/usr/bin/yay

#location of paru
PARU_LOCATION=/usr/bin/paru

#Installing Flatpak
function flatpak()
{
    if [[ "$package_manager" == "pacman" ]];
    then
        sudo pacman -S flatpak --noconfirm --needed
        # echo 'Arch System is not supported yet!!! Sorry'
        # exit 0
    elif [[ "$package_manager" == "apt-get" ]];
    then 
        if [ ! -e "$FLATPAK" ]; 
        then
            sudo apt install flatpak -y
            sudp apt-get update -y
            sudo apt-get upgrade -y
            sudo apt install gnome-software-plugin-flatpak -y
            sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo -y
            splash 'Flatpak is Installed'
        else  
            splash "Flatpak is in the System"
        fi
    else
        echo 'Error Occured: ${package_manager}'
        exit 0
    fi  
}
#Installing Snap Store
function snap_store()
{
    if [ ! -e "$SNAP" ]; 
    then
        sudo apt install snapd -y
        sudo snap install snap-store -y
        splash 'Snap Store is installed'
    else  
        splash "Snap Is already Installed"
    fi
}
function paru_check()
{
    if [ ! -e "$PARU_LOCATION" ]; 
    then
        git clone https://aur.archlinux.org/paru.git
        cd paru
        makepkg -si
        cd ../
        sudo pacman -S --needed base-devel --noconfirm --needed
        # sudo pacman -S yay --noconfirm --needed
        splash 'Paru is installed'
    else  
        splash 'Paru is already Installed'
    fi
}
#Installing Yay
function yay_function()
{   
    if [ ! -e "$YAY_LOCATION" ]; 
    then
        paru -S yay --noconfirm --needed
        splash 'Yay is installed'
    else  
        splash 'Yay is already Installed'
    fi
}

function fixes()
{
    if [[ "$package_manager" == "pacman" ]];
    then
        sudo pacman -Sy archlinux-keyring --noconfirm --needed
    elif [[ "$package_manager" == "apt-get" ]];
    then 
        sudo apt-get update --fix-missing
        sudo dpkg --configure -a
        sudo apt-get install -f
        sudo apt upgrade -y
    else
        echo 'Error Occured: ${package_manager}'
        exit 0
    fi
}


function add_repo_arch()
{
    #Add parallel downloading
    sudo sed -i 's/^#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf

    #Enable multilib
    sudo sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
}
function remove_password()
{
        # Add sudo no password rights
    echo "Removing Password from User Account"
    sudo sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
    sudo sed -i 's/^# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers
}
function update-arch()
{
    sudo pacman -Syyu --noconfirm --needed
}
function update_debian()
{
    sudo apt-get update -y
    sudo apt-get upgrade -y
}

function check_compoent()
{
   if [[ "$package_manager" == "pacman" ]];
    then
        remove_password
        fixes
        update-arch
        add_repo_arch
        update-arch
        paru_check
        yay_function
        update-arch
    elif [[ "$package_manager" == "apt-get" ]];
    then
        update_debian
        fixes 
        snap_store
        flatpak
        update_debian
    else
        echo 'Error Occured: ${package_manager}'
        exit 0
    fi
}

