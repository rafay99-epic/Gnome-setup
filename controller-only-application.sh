#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

#Importing .slash.sh file
. splash.sh

# this is very all of the package manager are placed
declare -A osInfo;
osInfo[/etc/debian_version]="apt-get"
osInfo[/etc/alpine-release]="apk"
osInfo[/etc/centos-release]="yum"
osInfo[/etc/fedora-release]="dnf"
osInfo[/etc/arch-release]="pacman"

#to find the which Os yo are running
for f in ${!osInfo[@]}
do
    if [[ -f $f ]];
    then
        package_manager=${osInfo[$f]}
    fi
done


function Display_message()
{
    splash 'Welcome To Application Installer
    Name: Mohammad Abdul Rafay 
    Email: 99marafay@gmail.com'
    echo ''
}
function permission_access()
{
    chmod +x check_os.sh
    chmod +x check_compoent.sh
    chmod +x controller_arch.sh
    chmod +x controller_debian.sh
}
function permission_remove()
{
    chmod -x check_os.sh
    chmod -x check_compoent.sh
    chmod -x controller_arch.sh
    chmod -x controller_debian.sh
}
function check_Access()
{
    #this will check for no-root
    if [ "$USER" = root ]; then
        echo_error "This script shouldn't be run as root. Aborting."
        echo_info "Run script like this:-   ./controller-only-application.sh"
        exit 1
    fi
}
# this is the Os function from this the debian or arch will run
function OS()
{
    if [[ "$package_manager" == "pacman" ]];
    then
        ./controller_arch.sh
    elif [[ "$package_manager" == "apt-get" ]];
    then
        sudo ./controller_debian.sh
    else
        echo_error 'Your OS is not supported.';
        echo_info 'try  ./controller-only-application';
        exit 1
    fi
}


function run() 
{
    #Checking  access if the application is running as a root ot not.
    check_Access   
    
    #givving Permissions
    permission_access
    
    #Displaying the Display messgae to the Terminal
    Display_message

    . check_compoent.sh
    check_compoent

    #checing Os and then passing the detail to the rest of the scripts
    OS

    #removing permissions
    permission_remove

}
run
