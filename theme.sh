#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

#Loading the splash screen
. splash.sh

#Loading the choose OS 
. check_os.sh

#Gnome Extension directory
EXTENSION_FOLDER=~/.local/share/gnome-shell/

#Gnome Theme Directory Location
USRTHEME=~/.local/share/gnome-shell/extensions/user-themegnome-shell-extensions.gcampax.github.com.v45.shell-extension
MENU_DRIVE=~/.local/share/gnome-shell/extensions/drive-menugnome-shell-extensions.gcampax.github.com.v47.shell-extension
FLOATING_DOCK=~/.local/share/gnome-shell/extensions/floating-docknandoferreira_profhotmail.com.v4.shell-extension
WORKSPACE=~/.local/share/gnome-shell/extensions/workspace-indicatorgnome-shell-extensions.gcampax.github.com.v48.shell-extension

#dirctory for themes folders
DIRECTORY=~/.themes
PICTURES=~/Pictures/wallpapers
ICONS=~/.icons

#The main Gnome Extension Function
function gnome_extensions()
{
    echo " "
    splash "Gnome Extensions"
    echo " "

    #placing all of the extension files
    placing_gnome_extension_Files

    #enabling the extensions
    enable_gnome_extensions
    
    #installing and enable flaot dock extension
    #float-extension    
}
# Placing the gnome extension files
function placing_gnome_extension_Files()
{
    if [ ! -d "$EXTENSION_FOLDER" ]; 
    then
        echo "Copying Files"
        cp -r ~/Gnome-setup/Gnome-extensions/* ~/.local/share/gnome-shell/extensions/
    else
    	cd ~
        cd ~/.local/share/gnome-shell
        mkdir extensions
        cp -r ~/Gnome-setup/Gnome-extensions/* ~/.local/share/gnome-shell/extensions/
    fi
    cd ~/Gnome-setup/
    
}

#Enabling the Gnome extensions Files
function enable_gnome_extensions()
{
    splash 'Enable Gnome Extensions'
    gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com
    gnome-extensions enable drive-menu@gnome-shell-extensions.gcampax.github.com
    gnome-extensions enable workspace-indicator@gnome-shell-extensions.gcampax.github.com    
}

#downloading the Flaoting dock in gnome and Installing the floating dock
function float-extension()
{
    git clone https://github.com/fer-moreira/floating-dock.git
    cd floating-dock
    make
    make install
}

# keeyboard shorcut for gnome Desktop Environment 
function keyboard_shortcut()
{
    echo " "
    splash 'Setting Up Keyboard Binding'
    echo " "

    splash 'Virtual Desktop Keyboard Binding'
    #shorcut for the switch between virtual desktop in Gnome
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 '["<Ctrl>1"]'
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 '["<Ctrl>2"]'
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 '["<Ctrl>3"]'
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 '["<Ctrl>4"]'
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-5 '["<Ctrl>5"]'
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-6 '["<Ctrl>6"]'
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-7 '["<Ctrl>7"]'
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-8 '["<Ctrl>8"]'
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-9 '["<Ctrl>9"]'


    BEGINNING="gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings"
    KEY_PATH="/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings"
    gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings \ "['$KEY_PATH/custom0/', '$KEY_PATH/custom1/', '$KEY_PATH/custom2/', '$KEY_PATH/custom3/', '$KEY_PATH/custom4/', '$KEY_PATH/custom5/']"

    # # Take a screenshot of the entire display
    # $BEGINNING/custom0/ name "Take Full Screenshot"
    # $BEGINNING/custom0/ command "shutter --full"
    # $BEGINNING/custom0/ binding "Print"

    # Clossing the current active window
    # $BEGINNING/custom1/ name "Close window"
    # $BEGINNING/custom1/ command "exit"
    # $BEGINNING/custom1/ binding "<Super>Q"

    echo " "
    splash "Kitty Terminal Keybinding"
    # Launch Terminal
    $BEGINNING/custom3/ name "Kitty Terminal"
    $BEGINNING/custom3/ command "kitty"
    $BEGINNING/custom3/ binding "<Super>T"

    echo " "
    splash "Home Folder Keybinding"
    # Open up file browser
    $BEGINNING/custom4/ name "Nautilus"
    $BEGINNING/custom4/ command "/usr/bin/nautilus --new-window"
    $BEGINNING/custom4/ binding "<Super>E"

    echo " "
    splash "Brave Broswer Keybinding"
    # Launch brave Browser
    $BEGINNING/custom5/ name "Brave"
    $BEGINNING/custom5/ command "brave-browser"
    $BEGINNING/custom5/ binding "<Super>B"

    
}


#The main Theme Function
function themes()
{
    echo " "
    splash "Welcome To Theme Section..."
    echo " "
    #downloading theme from github
    download_theme

    #This will move themes to the correct directory
    move_theme

    #which theme function will giver the user choice to select which theme ther are looking for
    which_theme

    #downloading and placing wallpapers and config files
    download_configFile_wallpaper

    #this will place the config files to the user .config flder
    set_config

    #Placing the  wallpapers
    place_wallpapers

    #setting up the wallpapers
    set_wallpaper

    #downloading Icon pack from github
    download_icons

    #enable icon pack
    enable_icons

    #Setting Keybinding
    keyboard_shortcut
}

#Downloading Themes BLue Sky
function download_theme()
{
    git clone https://github.com/i-mint/bluesky.git
    wget https://github.com/dracula/gtk/archive/master.zip
    unzip master.zip
}
#this function will move the themes folder to the correct location
function move_theme()
{
    if [[ -d "$DIRECTORY" ]]
    then
        echo "$DIRECTORY exists on your filesystem."
        mv   ~/Gnome-setup/bluesky/*  ~/.themes/
        mv ~/Gnome-setup/gtk-master/  ~/.themes/
    else
        cd ~
        mkdir .themes
        cd ~/Gnome-setup
        mv   ~/Gnome-setup/bluesky/*  ~/.themes/
        mv ~/Gnome-setup/gtk-master/  ~/.themes/
    fi
}

#this function will decide the which will be selected and the theme will be selected by the user
function which_theme()
{
    echo "Select Any of the Theme:"
    echo "1: Drackula Theme"
    echo "2. Blue Sky Darker"
    read -p 'Enter Your Choice: ' user_choice

    if [[ "$user_choice" == "1" ]];
    then 
        enable_drackula_theme
    else
        enable_blue_sky
    fi
}
#this function will enable the drackula theme
function enable_drackula_theme()
{
    echo " "
    splash 'Applying Drackula theme'
    echo " "
    #this will apply the drackula theme
    gsettings set org.gnome.desktop.interface gtk-theme "gtk-master"
    gsettings set org.gnome.desktop.wm.preferences theme "gtk-master"

    echo " "
    splash 'Changing Shell Theme'
    echo " "
    gsettings set org.gnome.shell.extensions.user-theme name "gtk-master"
}
#Enable the Theme Blue Sky Theme 
function enable_blue_sky()
{
    echo " "
    splash 'Applying Blue Sky theme'
    echo " "
    #gnome-shell-extension-tool -e user-themes
    gsettings set org.gnome.desktop.interface gtk-theme "BlueSky-Dark"
    gsettings set org.gnome.desktop.wm.preferences theme "BlueSky-Dark"

    echo " "
    splash 'Changing Shell Theme'
    echo " "
    gsettings set org.gnome.shell.extensions.user-theme name "BlueSky-Dark"
}

#Downloading config files and the wallpapers
function download_configFile_wallpaper()
{
    echo " "
    splash "Downloading Wallpapers and Config Files"
    echo " "
    #Downloading config file and wallpapers
    git clone https://github.com/rafay99-epic/Configuration-Files.git


}
#this function will set the config files to the .config folder
function set_config()
{
    #There are different config files for the arch system and debian system that is why which Os you are running.
    # if [[ "$package_manager" == "pacman" ]];
    # then
        #Placing config Files
        splash 'Placing Config Files in .config Folder'
        # copying config files from directory to .config files
        cp -r ~/Gnome-setup/Configuration-Files/dotfiles/ alacritty fish kitty neofetch   ~/.config/
        # comppying bashrc zsh to home directory
        cp -r ~/Gnome-setup/Configuration-Files/dotfiles/bashrc/*    ~/
        cp -r ~/Gnome-setup/Configuration-Files/dotfiles/zsh/*       ~/ 
        # copying starship promote in to the .config folder
        cp -r ~/Gnome-setup/Configuration-Files/dotfiles/starship/*    ~/.config    
        
    # elif [[ "$package_manager" == "apt-get" ]];
    # then
    #     #Placing config Files
    #     splash 'Placing Config Files in .config Folder'
    #     # copying config files from directory to .config files
    #     cp -r ~/Gnome-setup/Configuration-Files/dotfiles/debian/*  ~/.config/
    # else
    #     echo 'Error Occured: ${package_manager}'
    #     exit 0
    # fi

}

#Placing all of the wallpapers to the correct location
function place_wallpapers()
{
    # Placing the Wallpaper
    echo " "
    splash "Placing Wallpapers"
    echo " "
    #placing wallpapers.
    if [[ -d "$PICTURES" ]]
    then
        echo "$PICTURES exists on your filesystem."
        cp -r ~/Gnome-setup/Configuration-Files/wallpapers/* ~/Pictures/wallpapers/
    else
        cd ~
        cd Pictures
        mkdir wallpapers
        cd ~/Gnome-setup
        cp -r ~/Gnome-setup/Configuration-Files/wallpapers/* ~/Pictures/wallpapers/
    fi

    
}
#Setting the Wallpaper
function set_wallpaper()
{
    echo " "
    splash 'Setting Wallpapers'
    echo " "
    gsettings set org.gnome.desktop.background picture-uri ~/Pictures/wallpapers/wallpaper13.jpg
}

# Downloading the icon pack
function download_icons()
{
    echo " "
    splash 'Downloading Candy Icon Pack'
    echo " "
    #download Candy Icons Pack
    git clone https://github.com/EliverLara/candy-icons.git

    #Placing the Icons pack to the correct location
    echo " "
    splash "Placing icons pack Files in .icons folder"
    echo " "
    #placing Candy Icons pack
    if [[ -d "$ICONS" ]]
    then
        echo "$ICONS exists on your filesystem."
        mv   ~/Gnome-setup/candy-icons  ~/.icons/
    else
        cd ~
        mkdir .icons
        cd ~/Gnome-setup
        mv   ~/Gnome-setup/candy-icons  ~/.icons/
    fi
}
# Enable the icon pack
function enable_icons()
{
    echo " "
    splash 'Applying Candy Icons Pack'
    echo " "
    gsettings set org.gnome.desktop.interface icon-theme "candy-icons"
}

#This is where the Fish shell theme is needed to be set 
function theme_terminal()
{
    #Installing shell frame work and the theme
    splash 'Installing omf fish shell Framework'
    #installing omf framework
    curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install > install
    install --path=~/.local/share/omf --config=~/.config/omf
}

#Changing the default shell
function changing_Shell()
{
    if [[ "$package_manager" == "pacman" ]];
    then
        echo " "
        splash "Default Shell Changing"
        echo " "
        echo "Enter Your User Password to change your default shell"
        chsh -s /bin/fish  
    elif [[ "$package_manager" == "apt-get" ]];
    then
        echo " "
        splash "Default Shell Changing"
        echo " "
        echo "Enter Your User Password to change your default shell"
        chsh -s /usr/bin/fish    
    else
        echo 'Error Occured: ${package_manager}'
        exit 0
    fi
}
function shell()
{
    echo " "
    splash "Terminal Theme with Omf Fish FrameWork"
    echo " "

    #theme_terminal
    changing_Shell
    #Adding Omf theme
    theme_terminal
}


#The begining of the apply theme to gnome
function apply_themes()
{
    gnome_extensions
    themes
    shell
}
