#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

declare -A osInfo;
osInfo[/etc/debian_version]="apt-get"
osInfo[/etc/alpine-release]="apk"
osInfo[/etc/centos-release]="yum"
osInfo[/etc/fedora-release]="dnf"
osInfo[/etc/arch-release]="pacman"

#to find the which Os yo are running
for f in ${!osInfo[@]}
do
    if [[ -f $f ]];
    then
        package_manager=${osInfo[$f]}
    fi
done

function provide_permissions()
{
    echo -ne "
-------------------------------------------------------------------------
                Giving Permission to Files            
-------------------------------------------------------------------------
"

    chmod +x startup.sh
    chmod +x theme_starter.sh
    chmod +x check_os.sh
    chmod +x controller-only-application.sh

}

function reverse_permission()
{
    echo -ne "
-------------------------------------------------------------------------
        Removing permissions from Files
-------------------------------------------------------------------------
"
    # Removing The file Permissions
    chmod -x startup.sh    
    chmod -x theme_starter.sh
    chmod -x check_os.sh
    chmod -x controller-only-application.sh    
}
function goodbye_Message()
{
    echo -ne "
-------------------------------------------------------------------------
          GoodBye!! Have a Good Day
          Regard Author: Abdul Rafay              
-------------------------------------------------------------------------
" 
}
function clear_screen()
{
    clear
}
function non-root()
{
    if [ "$USER" = root ]; then
        echo -ne "
-------------------------------------------------------------------------
          This script shouldn't be run as root.

          Run script like this:-  ./install.sh
-------------------------------------------------------------------------
"
        exit 1
    fi

}
function reboot()
{
    # clear_screen
    goodbye_Message
    echo " "
    echo -ne "
-------------------------------------------------------------------------
                Reboot is Required 

        Enter Your Choice:
        
        Press Yes for a complete Reboot
        Press No to Abort & Exit Application

        Enter Your Choice:            
-------------------------------------------------------------------------
" 
    read -p  ' ' user_choice

    if [[ "$user_choice" == "yes" || "$user_choice" == "Yes" || "$user_choice" == "YES" || "$user_choice" == "yEs" || "$user_choice" == "yeS"  ]];
    then 
        reverse_permission
        # clear_screen
        # echo "Enter your user Password for reboot"
        echo -ne "
-------------------------------------------------------------------------
            Enter Password for Reboot                
-------------------------------------------------------------------------
"
        sudo reboot now
    elif [[ "$user_choice" == "no" || "$user_choice" == "No" || "$user_choice" == "nO" || "$user_choice" == "NO" ]];
    then
        reverse_permission
        clear_screen
        exit
    else
        reverse_permission
        clear_screen
        exit
    fi
}

function install_application()
{
    clear_screen
    echo -ne "
-------------------------------------------------------------------------
                Installing Application
-------------------------------------------------------------------------
"
    ./controller-only-application.sh
    echo " "
    echo -ne "
-------------------------------------------------------------------------
        All Application are Installed on this System        
-------------------------------------------------------------------------
"
    reboot
}
function desktop_supported()
{
    desktop=$(echo "$XDG_DATA_DIRS" | sed 's/.*\(xfce\|kde\|gnome\).*/\1/')
    clear_screen

    #giving Permissions
    provide_permissions
    clear_screen
    echo -ne "
-------------------------------------------------------------------------
                Desktop Environment is Supported
    
    Select from the following option
    
    1. Install Application & Customize Gnome
    2. Install Application Only
    3. Exit from Application
    
    Pick a Choice: 
-------------------------------------------------------------------------
"
    read -p ' ' choice
        
    if [[ "$choice" == '1' ]]
    then
         #checking which Os you are running
         . check_os.sh
         check_Os
        reverse_permission
        reboot
    elif [[ "$choice" == "2" ]]
    then
        install_application
        remove_permissions
        reboot
    else
        reverse_permission
        reboot
        
        exit 0
    fi
         
}
function Desktop_Not_Supported()
{
    echo -ne "
    -------------------------------------------------------------------------
                    Desktop Environment is Not Supported
                    Sorry!! Please Install Gnome Desktop      
    -------------------------------------------------------------------------
    "
    reverse_permission
    exit 0

}



#     desktop=$XDG_CURRENT_DESKTOP
#     clear_screen
    
#     echo -ne "
# -------------------------------------------------------------------------
#             Desktop Environment is Not Supported
    
#     Choose from the following option:
    
#     1. Install Gnome Desktop Environment
#     2. Install Only application for your System 
#     3. Exit from Application

#     Enter Your Choices:
# -------------------------------------------------------------------------
# "
#     read -p  ' ' choice
#     if [[ "$choice" == '1' ]]
#     then
#         install_dekstop_environment
#     echo -ne "
# -------------------------------------------------------------------------
#             Gnome Desktop Environment is Installed
    
#     Do you want to Customize Gnome Desktop Environment

#     Choose from the following option:
#     Press Yes: To start Customize Gnome
#     Press No: Exit the Application

#     Enter Your Choices:
# -------------------------------------------------------------------------
# "
#         read -p ' '  user_choice
        
#         if [[ "$user_choice" == "yes" || "$user_choice" == "Yes" || "$user_choice" == "YES" || "$user_choice" == "yEs" || "$user_choice" == "yeS"  ]];
#         then
#             check-desktop-evironment
#         else
#             reverse_permission
#             reboot
#         fi
#     elif [[ "$choice" == "2" ]]
#     then
#         # install_application
#         reboot
#     else
#         reverse_permission
#         reboot
#         exit 0
#     fi
#}


#this function will check for desktop environment and if the destop is not there then it will install desktop environment once installed then it will ask for the customization
function check-desktop-evironment()
{   
    if [ "$XDG_CURRENT_DESKTOP" = "GNOME" ]
    then
        desktop=$(echo "$XDG_DATA_DIRS" | sed 's/.*\(xfce\|kde\|gnome\).*/\1/')
        desktop_supported
    else
        Desktop_Not_Supported
    fi
}
function beginning()
{
    #script should run as non-root user
    non-root

    #providing Permisions to the files
    provide_permissions

    #checking your desktop environment
    check-desktop-evironment
}
beginning

