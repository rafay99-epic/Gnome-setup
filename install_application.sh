#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

# All of the Compoents Files 
. splash.sh
. check_os.sh
. install-application-Arch.sh
. install-application-Debian.sh

#Display Message
splash 'Installing Application for the system'

#Calling Function
function call()
{
    if [[ "$package_manager" == "pacman" ]];
    then
        install-application-arch
        splash 'All Applications are Installed'  
        #echo 'Arch System is not supported yet!!! Sorry'
        #exit 0
    elif [[ "$package_manager" == "apt-get" ]];
    then 
        install_application_debian 
        splash 'All Applications are Installed'      
    else
        echo 'Error Occured: ${package_manager}'
        exit 0
    fi

}

