#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

# Compoent Files 
. splash.sh
. check_os.sh

#Update Distro Function
function update()
{
    sudo apt-get update -y 
    sudo apt-get upgrade -y
}
function repo()
{
    #brave browser repo
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

    #Flathub repo
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    #Vs code repo
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
    sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
    sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
    rm -f packages.microsoft.gpg

    #wine Repo
    sudo dpkg --add-architecture i386

    #this repo is for the touchpad gestures for debian and Ubuntu
    sudo add-apt-repository ppa:touchegg/stable

    #Updating the system
    update
}
function package_Download() 
{
    echo " "
    splash "Downloading Github Desktop"
    echo " "
    #Github Desktop Package Link
    wget https://github.com/shiftkey/desktop/releases/download/release-2.9.3-linux3/GitHubDesktop-linux-2.9.3-linux3.deb

    echo " "
    splash "Downloading Google Chrome"
    echo " "
    #Google Chrome Deb package
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

    echo " "
    splash "Downloading Microsoft Teams"
    echo " "
    #Microsoft team Package
    wget https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/teams_1.3.00.5153_amd64.deb

    echo " "
    splash "Downloading Steam"
    echo " "
    #downloading Steam
    wget https://steamcdn-a.akamaihd.net/client/installer/steam.deb
    
    echo " "
    splash "downloading Virtual Box"
    echo " "
    #Download virtual orcal box
    wget https://download.virtualbox.org/virtualbox/6.1.30/virtualbox-6.1_6.1.30-148432~Debian~bullseye_amd64.deb

    echo " "
    splash "Downloading Xampp SQL Server"
    echo " "
    wget https://www.apachefriends.org/xampp-files/7.4.27/xampp-linux-x64-7.4.27-1-installer.run

    echo " "
    splash "Clone Auto-CPU-Freq"
    echo " "
    #CPU Freq Gethub file
    git clone https://github.com/AdnanHodzic/auto-cpufreq.git

    echo " "
    splash "Clone Fetch Master 6000"
    echo " "
    #Fetch master file
    git clone https://github.com/anhsirk0/fetch-master-6000.git

    # #Clonning a netwok bridge
    # echo " "
    # splash "Clonning Br10.xml bridge"
    # echo " "
    # git clone https://gitlab.com/eflinux/kvmarch.git

    #Clonning a netwok bridge
    echo " "
    splash "Top 5 Boot Loader"
    echo " "
    git clone https://github.com/ChrisTitusTech/Top-5-Bootloader-Themes.git
}
function touchpad()
{
    sudo systemctl enable touchegg.service
    sudo systemctl start touchegg
}
function virt-manager()
{
    sudo systemctl enable libvirtd.service
    sudo systemctl start libvirtd.service
    mv br10.xml ~/
    sudo virsh net-define br10.xml
    sudo virsh net-start br10
    sudo virsh net-autostart br10
}
# Install application for Debian System
function install()
{
    #apt repo
    PKGS=(
        'libreoffice'
        'python3'
        'htop'
        'neofetch'
        'timeshift'
        'gnome-shell-extensions'
        'gnome-tweaks'
        'cmake'
        'figlet'
        'fish'
        'kitty'
        'pkg-config'
        'libfreetype6-dev' 
        'libfontconfig1-dev'
        'libxcb-xfixes0-dev' 
        'libxkbcommon-dev'
        'chrome-gnome-shell'
        'brave-browser'
        'fonts-mononoki'
        'fonts-powerline'
        'fonts-font-awesome'
        'gdebi-core'
        'apt-transport-https'
        'openjdk-11-jdk'
        'qemu-kvm'
        'libvirt-clients'
        'libvirt-daemon-system'
        'bridge-utils'
        'virtinst'
        'libvirt-daemon'
        'virt-manager'
        'wine64'
        'winetricks'
        'code'
        'touchegg'
    )
    for PKG in "${PKGS[@]}"; do
        splash "INSTALLING: ${PKG}"
        sudo apt-get install "$PKG" -y
    done

    # Flatpak 
    FLAT=(
        'com.spotify.Client'
        'com.google.AndroidStudio'
        'com.wps.Office'
        'org.onlyoffice.desktopeditors'
        'com.bitwarden.desktop'
        'org.apache.netbeans'
    )

    for FLAT in "${FLAT[@]}"; do
        splash "INSTALLING: ${FLAT}"
        sudo flatpak install "$FLAT" -y
    done

    #snap repo
    SNAP=(
        'notion-snap'
    )

    for SNAP in "${SNAP[@]}"; do
        splash "INSTALLING: ${SNAP}"
        sudo snap install "$SNAP"
    done

    # install google chrome
    splash ' Installing Google Chrome'
    sudo dpkg -i google-chrome-stable_current_amd64.deb  

    # Installing the Alarcitty Terminal
    splash 'Installing Alarcitty Terminal'
    sudo dpkg -i Alacritty.deb 

    #Installing Lsd 
    splash 'Installing LSD'
    sudo dpkg -i lsd.deb

    #Installing Github desktop
    splash 'Installing Github Desktop'
    sudo gdebi GitHubDesktop-linux-2.9.3-linux3.deb

    #Installing Microsoft Teams desktop
    splash 'Installing Microsoft Teams Desktop'
    sudo dpkg -i teams_1.3.00.5153_amd64.deb
    
    #Installing steam
    splash 'Installing Steam'
    sudo dpkg -i steam.deb

    #Installing CPU-Fre
    splash 'Installing Auto-CPU-Freq'
    cd auto-cpufreq 
    sudo ./auto-cpufreq-installer
    cd ../

    #Insralling Grub Boot Loader
    splash "Configing Grub Bootloader"
    cd Top-5-Bootloader-Themes
    sudo ./install.sh
    cd ../

    #Installing Fm6000
    splash "Installing Fm6000"
    cd fetch-master-6000
    chmod +x install.sh
    sudo ./install.sh
    cd ../
    
    #Installing Xampp SQL Server
    splash "Installing Xampp SQL Server"
    chmod +x xampp-linux-x64-7.4.27-1-installer.run
    sudo ./xampp-linux-x64-7.4.27-1-installer.run

    #Installing Virtual Machine 
    splash "Installing Virtal Machine"
    sudo dpkg -i virtualbox-6.1_6.1.30-148432~Debian~bullseye_amd64.deb


}

#this function will fix all of the broken packages if any packages is broken 
function fix-broken()
{
    # sudo apt --fix-broken install
    # sudo apt-get update --fix-missing
    # sudo apt-get install -f
    # sudo apt-get autoremove
    sudo apt-get update --fix-missing
    sudo dpkg --configure -a
    sudo apt-get install -f
    sudo apt upgrade
}


#This is the begining of the Installing Application for debian System
function install_application_debian() 
{
    splash "Application Installing For Debian System"

    echo " "
    splash "Updating System"
    echo " "
    #updting System before installing applications
    update

    echo " "
    splash "Adding Repo for Debian System"
    echo " "
    #Adding all of the required repository
    repo
    
    echo " "
    splash "Downloading Packages"
    echo " "
    #Downloading all of the packages
    package_Download

    echo " "
    splash "Installing Applications"
    echo " "
    #Installing all of the Applications
    install

    echo " "
    splash "Updating System"
    echo " "
    #updating system one last time
    update

    #this is for the working for the virt-manager machine
    echo " "
    splash "Config The virtal Machine"
    echo " "
    virt-manager

    #this will enable and start the guestres for the touch pad.
    echo " "
    splash "Config the Touchpad Gestures"
    echo " "
    touchpad

    #Once all the application are installed and if incae any application is broken then Fix-broken function will be called to solveall of the issue.
    echo " "
    splash "Fixing any broken packages"
    echo " "
    fix-broken

    echo " "
    splash "Updating Debian System"
    echo " "
    #updating system one last time
    update

}

