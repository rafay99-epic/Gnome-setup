#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

#Location for Git,Curl,wget
CURL=/usr/bin/curl
GIT=/usr/bin/git
WGET=/usr/bin/wget

. splash.sh
. check_os.sh

function welcome_message()
{
    splash 'Welcome To Gnome Setup
    Name: Mohammad Abdul Rafay 
    Email: 99marafay@gmail.com'
    echo ''
}

function check_root()
{
    #this will check for root
    ROOT_UID=0
    if [[ ! "${UID}" -eq "${ROOT_UID}" ]]; then
        # Error message
        echo 'Run me as root, try sudo ./startup.sh' 
       # echo_error 'Run me as root.';
       # echo_info 'try sudo ./install.sh';
        exit 1
    fi
}

function give_permission()
{
    chmod +x check_compoent.sh
    chmod +x check_Internet.sh
    chmod +x splash.sh 
    chmod +x install_application.sh 
    chmod +x install-application-Arch.sh
    chmod + install-application-Debian.sh
}
function remove_permissions()
{
    chmod -x check_compoent.sh
    chmod -x check_Internet.sh
    chmod -x splash.sh 
    chmod -x install_application.sh
    chmod -x install-application-Arch.sh
    chmod -x install-application-Debian.sh
}

function curl_arch()
{
    if [ ! -e "$CURL" ]; 
    then
        sudo pacman -S curl --noconfirm --needed
        splash 'CURL is Installed'
    else  
        splash "CURL is in the System"
    fi
}
function git_arch()
{
    if [ ! -e "$GIT" ]; 
    then
        sudo pacman -S git --noconfirm --needed
        splash 'Git is Installed'
    else  
        splash "Git is in the System"
    fi
}
function wget_arch()
{
    if [ ! -e "$WGET" ]; 
    then
        sudo pacman -S wget --noconfirm --needed
        splash 'wget is Installed'
    else  
        splash "wget is in the System"
    fi
}

function curl_debian()
{
    if [ ! -e "$CURL" ]; 
    then
        sudo apt install curl -y
        splash 'CURL is Installed'
    else  
        splash "CURL is in the System"
    fi
}
function git_debian()
{
if [ ! -e "$GIT" ]; 
    then
        sudo apt install git -y
        splash 'Git is Installed'
    else  
        splash "Git is in the System"
    fi
}
function wget_debian()
{
if [ ! -e "$WGET" ]; 
    then
        sudo apt install wget -y
        splash 'wget is Installed'
    else  
        splash "wget is in the System"
    fi
}
function download() 
{
    if [[ "$package_manager" == "pacman" ]];
    then
        curl_arch
        git_arch
        wget_arch
    elif [[ "$package_manager" == "apt-get" ]];
    then
        curl_debian
        git_debian
        wget_debian
    else
        echo 'Error Occured: ${package_manager}'
        exit 0
    fi
}

function get_start() 
{
    #checking for the root
    check_root

    #Display welcome message
    welcome_message

    #giving the permissions to the files
    give_permission

    #downloading git, curl and wget before the application is being launched
    download

    #decide which Os you have
    #check_Os
    echo " "
    splash 'Checking Internet Connection'
    . check_Internet.sh
    echo " "
    #removing files remove_permissions
    remove_permissions

}
get_start