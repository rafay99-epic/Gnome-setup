#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

#Importing .slash.sh file
. splash.sh

function update-arch()
{
        sudo pacman -Syyu --noconfirm --needed
        yay -Syyu --noconfirm --needed
}
#This function will config the virt manager
function virt-manager-arch()
{
        sudo systemctl enable libvirtd.service
        sudo systemctl start libvirtd.service
        cp br10.xml ~/
        sudo virsh net-define br10.xml
        sudo virsh net-start br10
        sudo virsh net-autostart br10
}
    
    #This function willo enable the gestures for trackpad on X11 Display Manager on arch Linux.
function trackpad-gestures()
{   
        sudo systemctl enable touchegg.service
        sudo systemctl start touchegg
}

function download_package()
{
        echo " "
        splash "Downloading Fetch-Master-6000"
        echo " "
        git clone https://github.com/anhsirk0/fetch-master-6000.git
    }
    function arch_application()
    {
        #Using pacman 
        application_pacman=(
            'htop'
            'neofetch'
            'fish'
            'kitty'
            'curl'
            'alacritty'
            'lsd'
            'netbeans'
            'virt-manager'
            'qemu'
            'qemu-arch-extra'
            'ovmf'
            'vde2'
            'ebtables'
            'dnsmasq'
            'bridge-utils'
            'openbsd-netcat'
            'discord'
            'nodejs'
            'npm'
        )
        #The installing of application
        for application_pacman in "${application_pacman[@]}"; do
            splash "INSTALLING: ${application_pacman}"
            sudo pacman -S "$application_pacman" --noconfirm --needed
        done

        #yay 
        application_yay=(
            'notion-app'
            'teams'
            'microsoft-edge-stable-bin'
            'auto-cpufreq'
            'xampp'
            'brave-bin'
            'onlyoffice-bin'
        )

        #The installing of application using yay
        for application_yay in "${application_yay[@]}"; do
            splash "INSTALLING: ${application_yay}"
            yay -S  "$application_yay" --noconfirm --needed
        done

        
        splash "Installing Fm6000"
        cd fetch-master-6000
        chmod +x install.sh
        sudo ./install.sh
    }

function arch()
{
        splash "Application Installing For Arch System Only"

        #calling update system
        echo " "
        splash "Updating Arch System"
        echo " "
        update-arch
        
        #This will call the download packages and some of the packages will be downloaed for Installing.
        echo " "
        splash "Download Packages"
        echo " "
        download_package

        # Calling the function to install applications
        echo " "
        splash "Installing Applications for Arch System"
        echo " "
        arch_application
        
        #configing the virt ThemeManager
        echo " "
        splash "Config the Virt Manager"
        echo " "
        virt-manager-arch

        #This will enable the gestures for trackpad.
        echo ""
        splash "Enabling the gestures"
        echo ""
        trackpad-gestures
}

# Running this script
arch 