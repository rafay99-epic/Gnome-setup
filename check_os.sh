#!/bin/bash

#/**
# * Mohammad Abdul Rafay Automate Task for Linux
# * Email: 99marafay@gmail.com
# */

#Importing .slash.sh file
. splash.sh


# this is very all of the package manager are placed
declare -A osInfo;
osInfo[/etc/debian_version]="apt-get"
osInfo[/etc/alpine-release]="apk"
osInfo[/etc/centos-release]="yum"
osInfo[/etc/fedora-release]="dnf"
osInfo[/etc/arch-release]="pacman"

#to find the which Os yo are running
for f in ${!osInfo[@]}
do
    if [[ -f $f ]];
    then
         package_manager=${osInfo[$f]}
    fi
done

function check_Os()
{
    if [[ "$package_manager" == "pacman" ]];
    then
        #Starting up the files toinstall applications
        sudo ./startup.sh
        #Starting the teme part and that will config the visual styles
        ./theme_starter.sh
        # splash 'Arch System'
        # echo 'Arch System is not supported yet!!! Sorry'
        # exit 0
    elif [[ "$package_manager" == "apt-get" ]];
    then
        #starting up the files to install applications
        sudo ./startup.sh
        #Running the Files
        ./theme_starter.sh

        ## This is the working part of the application.

        #. check_desktop-Envirnoment.sh
        #check_desktop_environment

        # if [[ "$XDG_CURRENT_DESKTOP" == "GNOME" ]];
        # then
        #     echo " " 
        #     splash 'Debian System'
        #     echo " "
        #     #starting up the files to install applications
        #     sudo ./startup.sh
        #     #Running the Files
        #     ./theme_starter.sh
        # else 
        #     echo "The desktop Environment is not supported the Application will work only Gnome"
        #     . install.sh
        #     reverse_permission
        #     exit 1
        # fi
    else
        echo 'Error Occured: ${package_manager}'
        exit 0
    fi
}
